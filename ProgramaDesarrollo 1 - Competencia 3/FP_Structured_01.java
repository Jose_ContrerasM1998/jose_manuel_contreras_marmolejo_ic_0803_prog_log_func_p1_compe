import java.util.List;

/* Structured Approach */

public class FP_Structured_01{

	public static void main(String[] args) {

	    List<Integer> numbers = List.of(120, 91, 136, 45, 68, 222, 414, 122, 158);

	    FP_Structured_01.printAllNumbersInListStructured(numbers);
	    System.out.println("");
	    FP_Structured_01.printEvenNumbersInListStructured(numbers);
	    System.out.println("");

	    printAllNumbersInListStructured(List.of(250, 910, 1366, 450, 689, 1222, 2414, 8122, 4158));
	    System.out.println("");
	    printEvenNumbersInListStructured(List.of(1, 69, 46, 78, 10, 15, 55, 1250, 18));
	    System.out.println("");	
	}

	private static void printAllNumbersInListStructured(List<Integer> numbers){
		// How to loop the numbers?
		for(int number : numbers){
			System.out.print(number + ", ");
		}
		System.out.println("");
	}

	private static void printEvenNumbersInListStructured(List<Integer> numbers) {
		// How to loop the numbers?
		for(int number : numbers){
			if(number % 6 == 0){
				System.out.print(number + ", ");
			}
		}
		System.out.println("");
	}
	
}
